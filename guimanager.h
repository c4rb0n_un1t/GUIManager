#pragma once

#include <QtCore>
#include <QWidget>

#include "../../../Application/Interfaces/iapplication.h"
#include "../../Common/GUIElement/iguielement.h"
#include "../../Interfaces/ipluginlinker.h"
#include "../../Common/Plugin/plugin_base.h"

//! \addtogroup GUIManager_imp
//!  \{
class GUIManager : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "PLAG.Plugin" FILE "PluginMeta.json")
	Q_INTERFACES(
	        IPlugin
	)

public:
	GUIManager();
	virtual ~GUIManager() override;

	// QObject interface
public:
	bool eventFilter(QObject *watched, QEvent *event) override;

	// PluginBase interface
public:
	void onReady() override;
	virtual void onReferencesListUpdated(Interface interface) override;

signals:
	void onPop();

private slots:
	void onOpenLink(quint32 selfUID, quint32 referenceUID);
	void onCloseLink(quint32 selfUID, quint32 referenceUID);
	void onCloseSelf(quint32 selfUID);
	void onUserAnswered(quint32 askId, quint16 optionIndex);

private:
	bool registerUIElement(ReferenceInstancePtr<IGUIElement>& uiElement);
	bool unregisterUIElement(quint32 uid);
	void closeAllAndOpenRoot();
	void resolveConflict();
	inline quint32 getActiveElementUID()
	{
		return m_elementsStack.last();
	}
	inline ReferenceInstancePtr<IGUIElement>& getActiveElement()
	{
		return m_elementsMap[getActiveElementUID()];
	}

private:
	ReferenceInstancePtr<IApplication> m_app;
	ReferenceInstancePtr<IPluginLinker> m_pluginLinker;
	ReferenceInstancesListPtr<IGUIElement> m_uiElementsList;
	QWidget *m_parentWidget;
	quint32 m_rootElementUID;
	quint32 m_mainMenuAskId;

	QList<quint32> m_elementsStack;
	QMap<quint32, ReferenceInstancePtr<IGUIElement> > m_elementsMap;
	QMap<QString, QList<quint32> > m_elementLinksByNameMap;
	QList<quint32> m_mainMenuElementIds;
	bool m_isMainMenuConflictResolved;
	void linkElements();
};
//!  \}

